import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:switch_themes/core/core.dart';

part 'theme_event.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeData> {
  ThemeBloc() : super(lightThemeData()) {
    //when app is started
    on<InitialThemeSetEvent>((event, emit) async {
      final bool hasDarkTheme = await isDark();
      if (hasDarkTheme) {
        emit(darkThemeData());
      } else {
        emit(lightThemeData());
      }
    });

    //while switch is clicked
    on<ThemeSwitchEvent>((event, emit) {
      final isDark = state == darkThemeData();
      emit(isDark ? lightThemeData() : darkThemeData());
      setTheme(isDark);
    });
  }
}
