import 'package:flutter/material.dart';

ThemeData darkThemeData() {
  return ThemeData.dark(
    useMaterial3: true,
  );
}
