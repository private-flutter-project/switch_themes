import 'package:shared_preferences/shared_preferences.dart';
import 'package:switch_themes/core/consts/app_key.dart';

Future<bool> isDark() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool(AppKey.APP_THEME_MODE) ?? false;
}

Future<void> setTheme(bool isDark) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(AppKey.APP_THEME_MODE, !isDark);
}
